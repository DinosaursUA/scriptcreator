#include <windows.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <vector>
#pragma once
using namespace std;

HANDLE *hEvent1;
HANDLE *hEvent2;
HANDLE *hThr;
//HANDLE hSem;
unsigned long *uThrID;
int counterDot;			// ���������� �����

int sizeXY[2];

typedef struct MsTree_t{
	MsTree_t *parent;
	int XY[2];
	vector <MsTree_t> childs;
} MsTree_t;

typedef struct Count{
	int index;
} Count;

typedef struct{
	bool loadThread;	//������� ������ ��� ���
	int idThread;		//������������� ����������� �������
	bool direction[4];	//0 - UP/DOWN (true/false); 1 - LEFT/RIGHT (true/false); 2 - USE[0]; 3 - USE[1]
	MsTree_t* pos;		//��������� � ��������� � ����������� �������
	MsTree_t* ret;		//��������� � ����� �������� ��������
} MsParams_t;

typedef struct{
	bool direction[4];	//0 - UP/DOWN (true/false); 1 - LEFT/RIGHT (true/false); 2 - USE[0]; 3 - USE[1]
	MsTree_t pos;		//��������� � ��������� � ����������� �������
} MsMath_t;

void CreateDirection(int i, MsMath_t* data);
void Thread(void* pParams);
void Test(int index);
void StartMenu();
void LoadThread(int count, MsParams_t* param, Count* co);
void CreateTask(int counterDot, MsMath_t* math_t);
bool SolvedingTasks(int count, int cd, MsMath_t* math_t, MsParams_t* param);
void CloseAllHendl(int hCounts);
void SaveData();