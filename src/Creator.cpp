#include "Creator.h"
MsTree_t MasterTree;
MsParams_t *params;


int main(void) {
	//����������� ���������� ���� � �������
	SYSTEM_INFO sysinfo;
	GetSystemInfo(&sysinfo);
	int count = sysinfo.dwNumberOfProcessors;
	//�������� ������� �� ���������� ����
	hEvent1 = new HANDLE[count];
	hEvent2 = new HANDLE[count];
	hThr = new HANDLE[count];
	uThrID = new unsigned long[count];
	params = new MsParams_t[count];
	Count *counerThreadId = new Count[count];
	MasterTree.parent = NULL;
	//���� ������� ����������
	StartMenu();
	//������ �������
	LoadThread(count, params, counerThreadId);
	//��������� ��������
	int iDeph = 0;
	int cd;
	counterDot = 1;
	bool reload = false;
	do{
		// �������� ������������ ���������� �����
		cd = counterDot * 8;
		char menuChar;
		MsMath_t *math_t = new MsMath_t[cd];
		CreateTask(counterDot, math_t);
		//������� ������
		reload = SolvedingTasks(count, cd, math_t, params);
		iDeph++;
		cout << "deph of " << iDeph << " is don" << endl;
		cout << "continue(y) or save(other char):";
		cin >> menuChar;
		if (menuChar != 'y') {
			reload = false;
		}
	} while (reload);
	CloseAllHendl(count);
	SaveData();
	return 0;
}
/*
������� ������� ��������� � ����� ������� �����
�������� �������� ��� �������� �������� �����
*/
void CreateDirection(int i, MsMath_t* param){
	switch (i) {
	case 0:		//UP-LEFT
		param->direction[0] = true;
		param->direction[1] = true;
		param->direction[2] = true;
		param->direction[3] = true;
		break;
	case 1:		//DOWN-RIGHT
		param->direction[0] = false;
		param->direction[1] = false;
		param->direction[2] = true;
		param->direction[3] = true;
		break;
	case 2:		//UP-RIGHT
		param->direction[0] = true;
		param->direction[1] = false;
		param->direction[2] = true;
		param->direction[3] = true;
		break;
	case 3:		//DOWN-LEFT
		param->direction[0] = false;
		param->direction[1] = true;
		param->direction[2] = true;
		param->direction[3] = true;
		break;
	case 4:		//UP
		param->direction[0] = true;
		param->direction[1] = true;	//unuse
		param->direction[2] = true;
		param->direction[3] = false;
		break;
	case 5:		//DOWN
		param->direction[0] = false;
		param->direction[1] = true;	//unuse
		param->direction[2] = true;
		param->direction[3] = false;
		break;
	case 6:		//LEFT
		param->direction[0] = true;	//unuse
		param->direction[1] = true;
		param->direction[2] = false;
		param->direction[3] = true;
		break;
	case 7:		//RIGHT
		param->direction[0] = true;	//unuse
		param->direction[1] = false;
		param->direction[2] = false;
		param->direction[3] = true;
		break;
	}
}
/*
������� ����������� ���� ��� ���������� �����������
���������� ����������
*/
void StartMenu(){
	bool menu = true;
	cout << "Inputting size array" << endl;
	while (menu){
		cout << "X: ";
		cin >> sizeXY[0];
		if (sizeXY[0] > 4 && sizeXY[0] < 9) menu = false;
		else cout << "Error input varible" << endl;
	}
	menu = true;
	while (menu){
		cout << "Y: ";
		cin >> sizeXY[1];
		if (sizeXY[1] > 4 && sizeXY[1] < 9) menu = false;
		else cout << "Error input varible" << endl;
	}
	menu = true;
	cout << "Select start position" << endl;
	while (menu){
		cout << "(1 - " << sizeXY[0] << ")" << endl;
		cout << "X: ";
		cin >> MasterTree.XY[0];
		if (MasterTree.XY[0] <= sizeXY[0] && MasterTree.XY[0] > 0) menu = false;
		else cout << "Error input varible" << endl;
	}
	menu = true;
	while (menu){
		cout << "(1 - " << sizeXY[1] << ")" << endl;
		cout << "Y: ";
		cin >> MasterTree.XY[1];
		if (MasterTree.XY[1] <= sizeXY[1] && MasterTree.XY[1] > 0) menu = false;
		else cout << "Error input varible" << endl;
	}
}
/*
������� ��� ������� ���� ������� � ���������� �� �
���������� ��������. ����������� ������� ������ �
������� ������������
*/
void LoadThread(int count, MsParams_t* param, Count* co){
	//hSem = CreateSemaphore(NULL, 0, count, L"MySemaphore1");
	for (int i = 0; i < count; i++){
		param[i].idThread = i;
		co[i].index = i;
		hEvent1[i] = CreateEvent(NULL, FALSE, FALSE, NULL);
		hEvent2[i] = CreateEvent(NULL, FALSE, FALSE, NULL);
		hThr[i] = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)Thread, (void*)&co[i], 0, &uThrID[i]);
		Sleep(10);
	}
}
/*
������� �������� ���� ������� � ������ ��������
*/
void CloseAllHendl(int hCounts){
	for (int i = 0; i < hCounts; i++){
		CloseHandle(hEvent1[i]);
		CloseHandle(hEvent2[i]);
		CloseHandle(hThr[i]);
	}
}
/*
������� ��� �������� ������� ����� �� ������ 
���������� ��������� ����� � �� �������
*/
void CreateTask(int counterDot, MsMath_t* math_t){
	for (int i = 0; i < counterDot; i++){
		MsTree_t pos;
		if (MasterTree.childs.size() < 1) pos = MasterTree;
		else pos = MasterTree.childs[i];
		for (int j = 0; j < 8; j++){
			math_t[8 * i + j].pos = pos;
			CreateDirection(j, &math_t[8 * i + j]);
		}
	}
}
/*
������� ������������ ������ � ������ �� ������� 
� �������, ��������� �������� ����������� � 
������ ��� ���������� ���������
*/
bool SolvedingTasks(int count, int cd, MsMath_t* math_t, MsParams_t* param){
	bool fullRoad = false;	// ���������� ��� �������� ��������, ���� ������, �� ������ ��� ����� �������
	bool prior_to = true;	// ���������� ������������ ��� �� ������ ���� ������ � ����������� ����
	bool clear =	false;	// ���������� ������������ ��������� �� ���� child � ��������� ������
	int solvedTask = 0;		// ������� �������� �����
	do{
		for (int i = 0; i < count; i++){
			if (i < (cd - solvedTask)){
				param[i].loadThread = true;
				param[i].direction[0] = math_t[i + solvedTask].direction[0];
				param[i].direction[1] = math_t[i + solvedTask].direction[1];
				param[i].direction[2] = math_t[i + solvedTask].direction[2];
				param[i].direction[3] = math_t[i + solvedTask].direction[3];
				param[i].pos = &math_t[i + solvedTask].pos;
			}
			else{
				param[i].loadThread = false;
			}
			SetEvent(hEvent1[i]);
		}
		// �������� ���������� ���� ���������� �����
		WaitForMultipleObjects(count, hEvent2, true, INFINITE);
		//������ ��������� �������
		if (!clear) {
			MasterTree.childs.clear();
			counterDot = 0;
			clear = true;
		}
		for (int i = 0; i < count; i++){
			if (param[i].loadThread == true){
				counterDot++;
				fullRoad = true;
				//param[i].ret->parent = math_t[i + solvedTask].pos;
				MsTree_t newData;
				newData.parent = &math_t[i + solvedTask].pos;
				newData.XY[0] = param[i].ret->XY[0];
				newData.XY[1] = param[i].ret->XY[1];
				MasterTree.childs.push_back(newData);
			}
			else{
				if (&math_t[i + solvedTask].pos != NULL){
					//counterDot++;
				}
			}
		}
		solvedTask += count;
		if (solvedTask >= cd) prior_to = false;
	} while (prior_to);
	return fullRoad;
}
/*
������� ���������� � ��������� ���� ����������
�������
*/
void SaveData(){
	int saveTasks = MasterTree.childs.size();
	char* namefile;
	int a = sizeXY[0]*10+sizeXY[1];		//�������� �����
	char buffer[10];					//���������
	namefile = itoa(a, buffer, 10);		//�������������� � ������	
	ofstream out(namefile);
	MsTree_t *load;
	for (int i = 0; i < saveTasks; i++){
		load = &MasterTree.childs[i];
		while (load->parent != NULL){
			out << load->XY[0] << "|" << load->XY[1] << ",";
			load = load->parent;
		}
		out << MasterTree.XY[0] << "|" << MasterTree.XY[1];
		out << "\n";
	}
	out.close();
}
/*
������� ������� �������������� ��� ������� ���� 
��������� ����� ������������ �������� �����
*/
void Thread(void* pParams) {
	Count * ptr = (Count *)pParams;
	int indexThread = ptr->index;
	cout << "Thread " << indexThread << " is loaded" << endl;
	//�������� ����������� �������
	int *localSizeXY = new int[2];
	localSizeXY[0] = sizeXY[0] + 2;
	localSizeXY[1] = sizeXY[1] + 2;
	//�������� ������ �������
	int **localArray = new int*[localSizeXY[0]];
	for (int i = 0; i < localSizeXY[0]; i++){
		localArray[i] = new int[localSizeXY[1]];
	}
	// ���������� ����
	while (true) {
		// �������� ������� ������
		WaitForSingleObject(hEvent1[indexThread], INFINITE);
		// ��������� ������ ��� ���
		if (params[indexThread].loadThread) {
			//�������� ����
			for (int i = 0; i < localSizeXY[0]; i++){
				for (int j = 0; j < localSizeXY[1]; j++){
					//���������� ������� 0, � �� ������ -1
					if (i == 0 || j == 0 || i == (localSizeXY[0] - 1) || j == (localSizeXY[1] - 1)){
						localArray[i][j] = -1;
					}
					else localArray[i][j] = 0;
				}
			}
			MsTree_t *load = params[indexThread].pos->parent;
			while (load != NULL){
				if (load != NULL){
					localArray[load->XY[0]][load->XY[1]] = -1;
					load = load->parent;
				}
			}
			//������ ����� �����
			int *loadXY = new int[2];
			int *newXY = new int[2];
			loadXY[0] = params[indexThread].pos->XY[0];
			loadXY[1] = params[indexThread].pos->XY[1];
			int stap = localArray[loadXY[0]][loadXY[1]];
			while (stap != -1){
				newXY[0] = loadXY[0];	//�������� ���������� �����
				newXY[1] = loadXY[1];	//������������� �� -1
				if (params[indexThread].direction[2]){
					if (params[indexThread].direction[0])
						loadXY[0]--;
					else
						loadXY[0]++;
				}
				if (params[indexThread].direction[3]){
					if (params[indexThread].direction[1])
						loadXY[1]++;
					else
						loadXY[1]--;
				}
				stap = localArray[loadXY[0]][loadXY[1]];
			}
			// �������� ����� ���������
			if (newXY[0] != params[indexThread].pos->XY[0] || newXY[1] != params[indexThread].pos->XY[1]){
				MsTree_t newTree;
				newTree.parent = NULL;
				newTree.XY[0] = newXY[0];
				newTree.XY[1] = newXY[1];
				params[indexThread].ret = &newTree;
			}
			else{
				params[indexThread].loadThread = false;
			}
		}
		SetEvent(hEvent2[indexThread]);
	}
}